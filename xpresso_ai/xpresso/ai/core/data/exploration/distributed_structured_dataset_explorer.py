__all__ = ['DistributedStructuredDatasetExplorer']
__author__ = 'Srijan Sharma'

from xpresso.ai.core.logging.xpr_log import XprLogger

# This is indented as logger can not be serialized and can not be part
# of automl
logger = XprLogger()


class DistributedStructuredDatasetExplorer:

    def __init__(self, dataset):
        self.dataset = dataset
